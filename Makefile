run:
	@docker-compose up -d style-sage-api

run-api-uwsgi:
	@docker-compose run -p 8000:8000 style-sage-api --uwsgi

build:
	@docker-compose build style-sage-api

makemigrations:
	@docker-compose run -v ${PWD}/style_sage:/app style-sage-api --makemigrations

migrate:
	@docker-compose run -v ${PWD}/style_sage:/app style-sage-api --migrate

loaddata:
	@docker-compose run style-sage-api --loaddata

test:
	@docker-compose run -v ${PWD}/style_sage:/app style-sage-api --test

createsuperuser:
	@docker-compose run -v ${PWD}/style_sage:/app style-sage-api --createsuperuser

create-token:
	@docker-compose run -v ${PWD}/style_sage:/app style-sage-api --create-token

download-images:
	@docker-compose run -v ${PWD}/style_sage:/app style-sage-api --download-images
