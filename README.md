# StyleSageTest

Technical test for StyleSage.

## Requirements

* Docker, docker-compose (Ubuntu, macOS, not tested in Windows).

## Run service

Run style sage API (port 8000) service:

    make run

### Access to API

Go to your browser => http://localhost:8000/

### API token

First, you need to create an application user:

    make createsuperuser

Then, create a token for consuming some endpoints:

    make create-token

## API documentation

YAML OpenAPI file [`./docs/openapi.yml`](./docs/openapi.yml).

## Environment variables

See environment variables [here](./docs/ENVIRONMENT_VARIABLES.md).

## Technical Test Solution

### The test database

`chinook.db` sqlite database has been downloaded from [sqlitetutorial.net](https://www.sqlitetutorial.net/sqlite-sample-database/) and used to create models and dump data into some 
fixture files:
* [`artists.json`](./style_sage/artists/fixtures/artists.json)
* [`albums.json`](./style_sage/albums/fixtures/albums.json)
* [`tracks.json`](./style_sage/albums/fixtures/tracks.json)

### Exercise 1 - Data API

After running service using `make run`, access to the following endpoints:

* List of artists (public endpoint).
````
curl http://localhost:8000/artists
````
* List of albums with songs (restricted to authenticated users).
````
curl http://localhost:8000/albums -H 'Authorization: Token <token>'
````
* List of albums for one artist (restricted to authenticated users).
````
curl http://localhost:8000/albums?artist_id=<artist_id> -H 'Authorization: Token <token>'
````
* List of albums, including artist name, track count, total album duration, longest track duration 
and shortest track duration (restricted to authenticated users).
````
curl http://localhost:8000/albums?extended=true -H 'Authorization: Token <token>'
````

### Exercise 2 - Passphrase validation

After running service using `make run`, access to the following endpoints:

* Passphrase basic (public endpoint).
````
curl -X POST http://localhost:8000/passphrase/basic -H 'Content-Type: application/json' -d '{"passphrases":"aa bb cc dd ee\naa bb cc dd aa"}'
````
* Passphrase advanced (public endpoint).
````
curl -X POST http://localhost:8000/passphrase/advanced -H 'Content-Type: application/json' -d '{"passphrases":"abcde fghij\nabcde xyz ecdab"}'
````

### Exercise 3 - Web Scraping

First, download images for artists scraping [All Music](https://www.allmusic.com).

    make download-images

Note that some artist images may be not found.

All Musing scraper has been manually implemented using `requests` and `beautifulsoup4` libraries in 
a short period of time, but it could be improved in the future to be more robust and maintainable.

* List of artists with images (public endpoint).
````
curl http://localhost:8000/artists
````
* List of albums, including artist name, track count, total album duration, longest track duration 
and shortest track duration (restricted to authenticated users).
````
curl http://localhost:8000/albums?extended=true -H 'Authorization: Token <token>'
````
