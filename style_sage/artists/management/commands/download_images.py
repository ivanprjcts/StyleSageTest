import logging
from urllib.request import urlretrieve
from django.core.management.base import BaseCommand
from django.core.files.images import ImageFile
from urllib3 import disable_warnings
from utils.scrapers.all_music_scraper import AllMusicScraper, ArtistNotFound, ImageNotFound
from artists.models import Artist, ArtistImage

logger = logging.getLogger()


class Command(BaseCommand):
    help = 'Find and download the images for all artists in the artists table.'

    def handle(self, *args, **options):
        disable_warnings()

        total_errors = self._download_images()
        if total_errors == 0:
            self.stdout.write(self.style.SUCCESS('Artist images downloaded successfully!'))
        else:
            self.stdout.write(self.style.ERROR(f'{total_errors} images could not be downloaded!'))

    def _download_images(self):
        all_music_api = AllMusicScraper()

        total_errors = 0
        for artist in Artist.objects.filter(images__isnull=True):
            try:
                self.stdout.write(f'Downloading image for {artist.name} ...')
                image_path, _ = urlretrieve(all_music_api.get_artist(artist.name).image_url)
                ArtistImage.objects.create(
                    artist=artist,
                    image=ImageFile(open(image_path, 'rb'), name="image.jpg")
                )
            except (ImageNotFound, ArtistNotFound):
                total_errors += 1
                self.stdout.write(self.style.WARNING(f'{artist.name} image could not be found :('))
            except Exception as err:
                total_errors += 1
                logger.exception(err)

        return total_errors
