from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from artists.models import Artist
from artists.serializers import ArtistSerializer


class ArtistViewSet(ListModelMixin, GenericViewSet):
    """
    A viewset for viewing artist instances.
    """
    serializer_class = ArtistSerializer
    queryset = Artist.objects.prefetch_related('images')
