from django.test import TestCase
from django.test.client import RequestFactory
from rest_framework import status
from artists.views import ArtistViewSet
from artists.models import Artist


class ArtistViewSetTestCase(TestCase):
    def setUp(self):
        self.rf = RequestFactory()
        self.artist_view = ArtistViewSet.as_view({'get': 'list'})

    def _create_data(self):
        self.artist1 = Artist.objects.create(name="artist1")
        self.artist2 = Artist.objects.create(name="artist2")

    def test_list_empty_results(self):
        response = self.artist_view(request=self.rf.get(path='/path'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"count": 0, "next": None, "previous": None, "results": []}, response.data)

    def test_list_some_results(self):
        self._create_data()

        response = self.artist_view(request=self.rf.get(path='/path'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, response.data["count"])
        self.assertEqual(2, len(response.data["results"]))
        self.assertEqual(self.artist1.name, response.data["results"][0]["name"])
        self.assertEqual(self.artist2.name, response.data["results"][1]["name"])
