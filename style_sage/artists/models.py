from django.db import models


class Artist(models.Model):
    id = models.AutoField(db_column='ArtistId', primary_key=True)
    name = models.TextField(db_column='Name', max_length=120)

    class Meta:
        db_table = 'artists'


class ArtistImage(models.Model):
    id = models.AutoField(db_column='ArtistImageId', primary_key=True)
    artist = models.ForeignKey(
        Artist, on_delete=models.CASCADE, db_column='ArtistId', related_name='images'
    )
    image = models.ImageField(db_column='Image', upload_to='images')

    class Meta:
        db_table = 'artist_images'
