from rest_framework import serializers
from artists.models import Artist, ArtistImage


class ArtistImageSerializer(serializers.ModelSerializer):
    image_url = serializers.SerializerMethodField()

    def get_image_url(self, artist_image):
        request = self.context.get('request')
        image_url = artist_image.image.url
        return request.build_absolute_uri(image_url)

    class Meta:
        model = ArtistImage
        fields = ['id', 'image_url']


class ArtistSerializer(serializers.ModelSerializer):
    images = ArtistImageSerializer(many=True)

    class Meta:
        model = Artist
        fields = ['id', 'name', 'images']
