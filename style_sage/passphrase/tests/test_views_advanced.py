from django.test import TestCase
from django.test.client import RequestFactory
from rest_framework import status
from passphrase.views import PassphraseAdvancedView


class PassphraseAdvancedViewTestCase(TestCase):
    def setUp(self):
        self.rf = RequestFactory()
        self.view = PassphraseAdvancedView.as_view()

    def test_view_some_valid_passphrases(self):
        passphrases = "a ab abc abd abf abj\nabcde fghij\niiii oiii ooii oooi oooo"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 3}, response.data)

    def test_view_no_valid_passphrases(self):
        passphrases = "abcde xyz ecdab\noiii ioii iioi iiio\naa bb cc dd aa"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 0}, response.data)

    def test_view_valid_and_no_valid_passphrases(self):
        passphrases = "aa bb ee dd ae\noiii ioii iioi iiio\nam ab abc abd abf abj"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 2}, response.data)

    def test_view_valid_duplicated_passphrases(self):
        passphrases = "a ab abc abd abf abjab\na ab abc abd abf abjab"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 2}, response.data)

    def test_view_bad_request(self):
        passphrases = "a ab abc abd abf abj\nabcde fghij\niiii oiii ooii oooi oooo"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"key": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_passphrase(self):
        result = PassphraseAdvancedView().is_valid_passphrase("aaa bba eei d ae")
        self.assertTrue(result)

    def test_not_valid_passphrase(self):
        result = PassphraseAdvancedView().is_valid_passphrase("aabe bbb e dd eaba")
        self.assertFalse(result)
