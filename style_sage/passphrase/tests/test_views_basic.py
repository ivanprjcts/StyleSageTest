from django.test import TestCase
from django.test.client import RequestFactory
from rest_framework import status
from passphrase.views import PassphraseBasicView


class PassphraseBasicViewTestCase(TestCase):
    def setUp(self):
        self.rf = RequestFactory()
        self.view = PassphraseBasicView.as_view()

    def test_view_some_valid_passphrases(self):
        passphrases = "aa bb cc dd ee\naa bb cc dd aaa"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 2}, response.data)

    def test_view_no_valid_passphrases(self):
        passphrases = "aa bb ee dd ee\naa bb cc dd dd\naa bb cc dd aa"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 0}, response.data)

    def test_view_valid_and_no_valid_passphrases(self):
        passphrases = "aa bb ee dd ae\naaa bb cc dd bb\naa bb cc dd aaa"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 2}, response.data)

    def test_view_valid_duplicated_passphrases(self):
        passphrases = "aa bb ee dd ae\naa bb ee dd ae"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"passphrases": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"valid_passphrases": 2}, response.data)

    def test_view_bad_request(self):
        passphrases = "aa bb cc dd ee\naa bb cc dd aaa"
        response = self.view(
            request=self.rf.post(
                "/path",
                {"key": passphrases},
                content_type="application/json"
            )
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_passphrase(self):
        result = PassphraseBasicView().is_valid_passphrase("aa bb ee dd ae")
        self.assertTrue(result)

    def test_not_valid_passphrase(self):
        result = PassphraseBasicView().is_valid_passphrase("aa bb ee dd aa")
        self.assertFalse(result)
