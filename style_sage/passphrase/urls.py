from django.urls import path
from passphrase.views import PassphraseBasicView, PassphraseAdvancedView


urlpatterns = [
    path('basic', PassphraseBasicView.as_view()),
    path('advanced', PassphraseAdvancedView.as_view())
]
