from rest_framework import serializers


class PassphraseSerializer(serializers.Serializer):
    passphrases = serializers.CharField()
