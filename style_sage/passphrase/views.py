from rest_framework.views import APIView
from rest_framework.response import Response
from passphrase.serializers import PassphraseSerializer


class PassphraseBasicView(APIView):
    """
    View to check all valid passphrases according to validation policy.

    * Requires to implement 'is_valid_passphrase' method.
    """

    def is_valid_passphrase(self, passphrase):
        """
        A valid passphrase must contain no duplicate words.
        """
        words = passphrase.split()
        not_duplicated_words = set(words)
        return len(words) == len(not_duplicated_words)

    def post(self, request, format=None):
        """
        Return the number of valid passphrases.
        """
        serializer = PassphraseSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        valid_passphrases = 0
        for passphrase in serializer.validated_data["passphrases"].split('\n'):
            if self.is_valid_passphrase(passphrase):
                valid_passphrases += 1

        return Response({"valid_passphrases": valid_passphrases})


class PassphraseAdvancedView(PassphraseBasicView):
    def is_valid_passphrase(self, passphrase):
        """
        A valid passphrase must contain no two words that are anagrams of each other.
        """
        words = passphrase.split()
        sorted_words = ["".join(sorted(word)) for word in words]
        not_duplicated_sorted_words = set(sorted_words)
        return len(sorted_words) == len(not_duplicated_sorted_words)
