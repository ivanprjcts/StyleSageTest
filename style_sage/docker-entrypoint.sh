#!/bin/sh

set -e

export HTTP_SERVER_WORKERS=${HTTP_SERVER_WORKERS:-2}
export HTTP_SERVER_THREADS=${HTTP_SERVER_THREADS:-10}
export HTTP_SERVER_TIMEOUT=${HTTP_SERVER_TIMEOUT:-30}

while test $# -gt 0
do
    case "$1" in
        --runserver)
            python manage.py runserver 0.0.0.0:8000
            ;;
        --uwsgi)
            exec uwsgi --enable-threads --workers $HTTP_SERVER_WORKERS \
                 --threads $HTTP_SERVER_THREADS \
                 -t $HTTP_SERVER_TIMEOUT \
                 --http 0.0.0.0:8000 --module=style_sage.wsgi:application --master
            ;;
        --migrate)
            python manage.py migrate --no-input
            ;;
        --makemigrations)
            python manage.py makemigrations --no-input
            ;;
        --test)
            coverage run manage.py test
            coverage report
            ;;
       --loaddata)
            python manage.py loaddata artists albums tracks
            ;;
       --createsuperuser)
            python manage.py createsuperuser --no-input
            ;;
       --create-token)
            python manage.py drf_create_token $DJANGO_SUPERUSER_USERNAME
            ;;
       --download-images)
            python manage.py download_images
            ;;
        --*) echo "bad option $1"
            ;;
        *)
            $1
            ;;
    esac
    shift
done
