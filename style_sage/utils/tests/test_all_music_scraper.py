from unittest.mock import patch
from django.test import TestCase
from utils.scrapers.all_music_scraper import AllMusicScraper


SEARCH_ACDC_HTML = """
<section class="artist-results"><ul>
<li class="result" data-id="MN0000574772" data-text="AC/DC" data-url="/artist/ac-dc-mn0000574772">
<p class="name">AC/DC </p><p class="genres">Pop/Rock </p>
</li>
<li class="result" data-id="MN0003955381" data-text="ACDC" data-url="/artist/acdc-mn0003955381">
<p class="name">ACDC </p><p class="genres">Pop/Rock </p>
</li>
</ul></section>
"""


ACDC_DETAIL_HTML = """
<head><title>AC/DC Songs, Albums, Reviews, Bio & More | AllMusic</title>
<!-- preload page-level assets -->
<link rel="preload" as="image" href="http://domain.com/image.jpg">
<link rel="preload" as="image" href="https://rovimusic.rovicorp.com/image.jpg?c=Rn9LK_xGXts=&f=4">
</head>
"""


class MockHttpResponse:
    def __init__(self, text):
        self.text = text


class AllMusicScraperTestCase(TestCase):
    def setUp(self):
        self.api = AllMusicScraper()

    @patch("requests.get", side_effect=[
        MockHttpResponse(SEARCH_ACDC_HTML), MockHttpResponse(ACDC_DETAIL_HTML)
    ])
    def test_get_artist(self, *args):
        artist = self.api.get_artist("AC/DC")
        self.assertEqual("http://domain.com/image.jpg", artist.image_url)
