import re
import requests
from bs4 import BeautifulSoup


class ArtistNotFound(Exception):
    """Artist could not be found"""


class ImageNotFound(Exception):
    """Image could not be found"""


class Artist:
    def __init__(self, image_url):
        self.image_url = image_url


class AllMusicScraper:
    def default_headers(self):
        return {
            "x-requested-with": "XMLHttpRequest",
            "Referer": "https://www.allmusic.com/advanced-search",
            "User-Agent": "AllMusicScraper/1.0.0"
        }

    def _parse_artist_url(self, html_text):
        soup = BeautifulSoup(html_text, 'html.parser')
        first_list_item = soup.find('li', attrs={'data-url': True})
        if not first_list_item:
            raise ArtistNotFound()

        return first_list_item["data-url"]

    def _get_artist_url_path(self, artist_name):
        response = requests.get(
            f"https://www.allmusic.com/search/typeahead/artist/{artist_name}",
            headers=self.default_headers()
        )
        return self._parse_artist_url(response.text)

    def _parse_artist_image_url(self, html_text):
        soup = BeautifulSoup(html_text, 'html.parser')
        first_image_link = soup.find(
            'link', rel='preload', href=re.compile(r"image\.jpg")
        )
        if not first_image_link:
            raise ImageNotFound()

        return first_image_link["href"]

    def get_artist(self, artist_name):
        artist_url_path = self._get_artist_url_path(artist_name)
        response = requests.get(
            "https://www.allmusic.com" + artist_url_path,
            headers=self.default_headers()
        )
        return Artist(
            image_url=self._parse_artist_image_url(response.text)
        )
