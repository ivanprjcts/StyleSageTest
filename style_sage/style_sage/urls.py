from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework import routers
from artists.views import ArtistViewSet
from albums.views import AlbumViewSet


router = routers.SimpleRouter(trailing_slash=False)
router.register(r'artists', ArtistViewSet)
router.register(r'albums', AlbumViewSet)


urlpatterns = [
    path('passphrase/', include("passphrase.urls")),
]

urlpatterns += router.urls
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
