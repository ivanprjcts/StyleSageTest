from rest_framework import serializers
from albums.models import Album, Track
from artists.serializers import ArtistSerializer


class AlbumExtendedListSerializer(serializers.ModelSerializer):
    artist = ArtistSerializer()
    total_tracks = serializers.IntegerField()
    total_milliseconds = serializers.IntegerField()
    longest_track_milliseconds = serializers.IntegerField()
    shortest_track_milliseconds = serializers.IntegerField()

    class Meta:
        model = Album
        fields = [
            'id', 'title', 'artist', 'total_tracks', 'total_milliseconds',
            'longest_track_milliseconds', 'shortest_track_milliseconds'
        ]


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Track
        fields = ['id', 'name', 'milliseconds']


class AlbumListSerializer(serializers.ModelSerializer):
    tracks = TrackSerializer(many=True)

    class Meta:
        model = Album
        fields = ['id', 'title', 'tracks', 'artist_id']
