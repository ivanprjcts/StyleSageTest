from django.db.models import Count, Sum, Max, Min
from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated
from albums.models import Album, Track
from albums.serializers import AlbumListSerializer, AlbumExtendedListSerializer
from albums.filters import AlbumFilter


class AlbumViewSet(ListModelMixin, GenericViewSet):
    """
    A viewset for viewing album instances.
    """
    serializer_class = AlbumListSerializer
    queryset = Album.objects.prefetch_related('tracks')
    permission_classes = [IsAuthenticated]
    filterset_class = AlbumFilter

    def _is_extended(self):
        return self.request.query_params.get('extended', 'false').lower() == 'true'

    def get_serializer_class(self):
        if self._is_extended():
            return AlbumExtendedListSerializer

        return self.serializer_class

    def get_queryset(self):
        if self._is_extended():
            return Album.objects.select_related('artist').prefetch_related(
                'artist__images'
            ).annotate(
                total_tracks=Count('tracks'),
                total_milliseconds=Sum('tracks__milliseconds'),
                longest_track_milliseconds=Max('tracks__milliseconds'),
                shortest_track_milliseconds=Min('tracks__milliseconds')
            )

        return self.queryset
