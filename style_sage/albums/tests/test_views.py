from django.test import TestCase
from django.test.client import RequestFactory
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
from artists.models import Artist
from albums.views import AlbumViewSet
from albums.models import Album, Track


class AlbumViewSetTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="user", email="email@domain.com", password="password"
        )
        self.token = Token.objects.create(user=self.user)
        self.rf = RequestFactory()
        self.album_view = AlbumViewSet.as_view({'get': 'list'})

    def _create_data(self):
        self.artist1 = Artist.objects.create(name="artist1")
        self.album1 = Album.objects.create(title="album1", artist=self.artist1)
        self.track1 = Track.objects.create(name="track1", milliseconds=10, album=self.album1)
        self.track2 = Track.objects.create(name="track2", milliseconds=20, album=self.album1)
        self.artist2 = Artist.objects.create(name="artist2")
        self.album2 = Album.objects.create(title="album2", artist=self.artist2)

    def test_list_not_authorized(self):
        response = self.album_view(request=self.rf.get(path='/path'))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_empty_results(self):
        response = self.album_view(
            request=self.rf.get(path='/path', HTTP_AUTHORIZATION=f'Token {self.token.key}')
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual({"count": 0, "next": None, "previous": None, "results": []}, response.data)

    def test_list_some_results(self):
        self._create_data()

        response = self.album_view(
            request=self.rf.get(path='/path', HTTP_AUTHORIZATION=f'Token {self.token.key}')
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, response.data["count"])
        self.assertEqual(2, len(response.data["results"]))
        self.assertEqual(2, len(response.data["results"][0]["tracks"]))
        self.assertEqual(0, len(response.data["results"][1]["tracks"]))

    def test_list_some_results_filtered_by_artist(self):
        self._create_data()

        response = self.album_view(
            request=self.rf.get(
                '/path', {'artist_id': self.artist1.id},
                HTTP_AUTHORIZATION=f'Token {self.token.key}'
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, response.data["count"])
        self.assertEqual(1, len(response.data["results"]))
        self.assertEqual(2, len(response.data["results"][0]["tracks"]))

    def test_list_some_results_extended(self):
        self._create_data()

        response = self.album_view(
            request=self.rf.get(
                '/path', {'extended': True}, HTTP_AUTHORIZATION=f'Token {self.token.key}'
            )
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, response.data["count"])
        self.assertEqual(2, len(response.data["results"]))
        self.assertEqual("artist1", response.data["results"][0]["artist"]["name"])
        self.assertEqual(2, response.data["results"][0]["total_tracks"])
        self.assertEqual(30, response.data["results"][0]["total_milliseconds"])
        self.assertEqual(20, response.data["results"][0]["longest_track_milliseconds"])
        self.assertEqual(10, response.data["results"][0]["shortest_track_milliseconds"])
