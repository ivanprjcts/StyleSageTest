from django.db import models
from artists.models import Artist


class Album(models.Model):
    id = models.AutoField(db_column='AlbumId', primary_key=True)
    title = models.TextField(db_column='Title', max_length=160)
    artist = models.ForeignKey(
        Artist, on_delete=models.CASCADE, db_column='ArtistId', related_name='albums'
    )

    class Meta:
        db_table = 'albums'


class Track(models.Model):
    id = models.AutoField(db_column='TrackId', primary_key=True)
    name = models.TextField(db_column='Name', max_length=200)
    album = models.ForeignKey(
        Album, on_delete=models.CASCADE, db_column='AlbumId', related_name='tracks'
    )
    milliseconds = models.IntegerField(db_column='Milliseconds')

    class Meta:
        db_table = 'tracks'
