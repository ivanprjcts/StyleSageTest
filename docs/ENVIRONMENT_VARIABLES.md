# Environment Variables

This document contains a brief description of each of environment variable.

## Style Sage API

Sets the configuration for API component.

- `DJANGO_SECRET_KEY`. Django secret key.
- `DJANGO_DEBUG`. Django debug mode. By default, **0** value.
- `DJANGO_ALLOWED_HOSTS`. Django allowed hosts. Example, *localhost,domain.com*. By default, **localhost** value.
- `DB_NAME`. Database name.
- `LOG_LEVEL`. Log level. By default, **INFO** value.

### Create superuser

- `DJANGO_SUPERUSER_PASSWORD`. Superuser password.
- `DJANGO_SUPERUSER_USERNAME`. Superuser username.
- `DJANGO_SUPERUSER_EMAIL`. Superuser email.
